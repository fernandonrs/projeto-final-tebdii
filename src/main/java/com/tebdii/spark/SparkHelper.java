/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.spark;

import com.mongodb.spark.config.WriteConfig;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bson.Document;

/**
 * Interface com a classe de ajuda de acesso ao MongoDB pelo Spark
 * 
 * @author fernando
 */
public interface SparkHelper<T> {
    
    public JavaSparkContext sparkContext();
    public WriteConfig getMongoDBWriteConfig();
    public void saveDocumentOnMongoDB(JavaRDD<Document> docs);
    
}
