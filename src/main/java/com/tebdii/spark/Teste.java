/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.spark;

import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.rdd.api.java.JavaMongoRDD;
import com.tebdii.mongodb.models.TweetDocument;
import com.tebdii.services.TweetDocumentService;
import com.tebdii.twitter.TwitterHelper;
import com.tebdii.utilities.Utilities;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.TwitterException;

/**
 *
 * @author fernando
 */
@Log4j2
@Component
public class Teste {
    
    @Autowired
    private JavaSparkContext spark;
    @Autowired
    private Utilities utils;
    @Autowired
    private TwitterHelper twitter;
    @Autowired
    private TweetDocumentService service;

    @PostConstruct
    private void TesteInit() { }    
    
    public void executeSimpleTest() {
        List<Integer> integers = new ArrayList();
                
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(6);
        
        log.info(this.utils.log_info("Paralelizando com o Spark..."));
        JavaRDD<Integer> rdd = this.spark.parallelize(integers);
        log.info(this.utils.log_info("Executando com o Spark..."));
        rdd.foreach((t) -> {
            System.out.println("Test de número " + t + " ok!");
        });
    }
    
    public Long getTotalFromMongoDB() {
        log.info(this.utils.log_info("Lendo o MongoDb com o Spark..."));
        JavaMongoRDD<Document> rdd = MongoSpark.load(this.spark);
        log.info(this.utils.log_info("Leitura do MongoDb executada..."));
        
        return rdd.count();
    }
    
    public void testTwitter() throws TwitterException {
        log.info(this.utils.log_info("Conectando com o MongoDB..."));
        this.twitter.connect();
        log.info(this.utils.log_info("Conectado ao MongoDB..."));
        List<TweetDocument> list = this.twitter.readTweet("MongoDB", 5);
        log.info(this.utils.log_info("Twitter lido..."));
        List<TweetDocument> tweets = this.twitter.analyzeSentiment(list);
        log.info(this.utils.log_info("Análise de sentimento feita..."));
        
        this.service.save(tweets);
        
    }
    
}
