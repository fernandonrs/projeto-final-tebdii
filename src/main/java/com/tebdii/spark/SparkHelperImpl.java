/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.spark;

import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.WriteConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Classe de ajuda para comunicação com o MongoDB pelo Spark
 * 
 * @author fernando
 */
@Component
class SparkHelperImpl<T> implements SparkHelper<T> {

    @Autowired
    private JavaSparkContext spark;
    
    @Override
    public JavaSparkContext sparkContext() {
        return this.spark;
    }

    @Override
    public WriteConfig getMongoDBWriteConfig() {
        // Create a custom WriteConfig
        Map<String, String> writeOverrides = new HashMap<String, String>();
        //writeOverrides.put("collection", "TWITTER-SENTIMENT");
        writeOverrides.put("writeConcern.w", "majority");
        WriteConfig writeConfig = WriteConfig.create(this.spark).withOptions(writeOverrides);
        
        return writeConfig;
    }

    @Override
    public void saveDocumentOnMongoDB(JavaRDD<Document> docs) {
        MongoSpark.save(docs, this.getMongoDBWriteConfig());
    }
    
}
