/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configuração do Spark na aplicação
 * 
 * @author fernando
 */
@Configuration
@PropertySource("classpath:application.properties")
public class SparkConfig {
    
    @Value("${app.name:TEBDII-App}")
    private String appName;

    @Value("${spark.home}")
    private String sparkHome;

    @Value("${master.uri:local[*]}")
    private String masterUri;

    @Bean
    public SparkConf sparkConf() {
        SparkConf sparkConf = new SparkConf()
                .setAppName(appName)
                .setSparkHome(sparkHome)
                .setMaster(masterUri)
                .set("spark.mongodb.input.uri", 
                        "mongodb+srv://TEBDII-User:000@cluster-estudo.jztpd.mongodb.net/MYDATABASE")		 
                .set("spark.mongodb.output.uri",
                        "mongodb+srv://TEBDII-User:000@cluster-estudo.jztpd.mongodb.net/MYDATABASE")
                .set("spark.mongodb.input.collection", "TWITTER-SENTIMENT")
                .set("spark.mongodb.output.collection", "TWITTER-SENTIMENT");

        return sparkConf;
    }

    @Bean
    public JavaSparkContext javaSparkContext() {
        return new JavaSparkContext(sparkConf());
    }

    @Bean
    public SparkSession sparkSession() {
        return SparkSession
                .builder()
                .sparkContext(javaSparkContext().sc())
                .appName("Java Spark SQL basic example")
                .getOrCreate();
    }
    
}
