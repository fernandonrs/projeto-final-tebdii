/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Classe de utilidades em geral
 * 
 * @author fernando
 */
@Component
@PropertySource("classpath:application.properties")
public class Utilities {
    
    @Value("${app.log.info.template}")
    private String log_info_template;
    
    public String log_info(String info) {    
        return String.format(this.log_info_template, info);
    }
    
}
