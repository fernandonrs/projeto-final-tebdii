/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.services;

import com.tebdii.mongodb.models.TweetDocument;
import com.tebdii.mongodb.repositories.TweetDocumentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Classe de serviço para acesso aos serviços e repositórios
 * 
 * @author fernando
 */
@Service
public class TweetDocumentServiceImpl implements TweetDocumentService {

    @Autowired private TweetDocumentRepository repository;
    
    @Override
    public List<TweetDocument> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TweetDocument> findBy(String filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(List<TweetDocument> tweets) {
        this.repository.save(tweets);
    }
    
}
