/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.services;

import com.tebdii.mongodb.models.TweetDocument;
import java.util.List;

/**
 * Onerface para a classe de serviço para a entidade TweetDocument
 * @author fernando
 */
public interface TweetDocumentService {
    public List<TweetDocument> findAll();
    public List<TweetDocument> findBy(String filter);
    public void save(List<TweetDocument> tweets);
}
