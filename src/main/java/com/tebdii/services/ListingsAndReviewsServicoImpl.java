/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.services;

import com.tebdii.mongodb.models.ListingsAndReviews;
import com.tebdii.mongodb.repositories.ListingsAndReviewsRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author fernando
 */
@Service
public class ListingsAndReviewsServicoImpl implements ListingsAndReviewsService {
    
    @Autowired private ListingsAndReviewsRepository repo;
    
    @Override
    public List<ListingsAndReviews> findAll() {
        return this.repo.findAll();
    }
    
}
