/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.services;

import com.tebdii.mongodb.models.ListingsAndReviews;
import java.util.List;

/**
 *
 * @author fernando
 */
public interface ListingsAndReviewsService {
    
    public List<ListingsAndReviews> findAll();
    
}
