/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.twitter;

import com.tebdii.mongodb.models.TweetDocument;
import java.util.List;
import twitter4j.TwitterException;

/**
 * Interface para os métodos do twitter
 * 
 * @author fernando
 */
public interface TwitterHelper {
    
    public void connect();
    public List<TweetDocument> readTweet(String filterWord, int quantityTweet) throws TwitterException;
    public List<TweetDocument> analyzeSentiment(List<TweetDocument> tweets);
    
}
