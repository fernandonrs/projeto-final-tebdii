/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.twitter;

import com.tebdii.mongodb.models.TweetDocument;
import com.tebdii.utilities.Utilities;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import twitter4j.Status;

/**
 * Facilitador para a API do Twitter
 *
 * @author fernando
 */
@Log4j2
@Component
@PropertySource("classpath:application.properties")
class TwitterHelperImpl implements TwitterHelper{

    @Autowired
    private SentimentAnalyzer analyzer;
    @Autowired
    private Utilities utils;

    @Value("${twitter.consumer.key}")
    private String consumerKey;
    @Value("${twitter.consumer.secret}")
    private String consumerSecrete;
    @Value("${twitter.access.token}")
    private String accessToken;
    @Value("${twitter.access.token.secret}")
    private String accessTokenSecret;

    private Twitter twitter;

    public final String SENTIMENT_NEUTRAL = "Neutral";
    public final String SENTIMENT_NEGATIVE = "Negative";
    public final String SENTIMENT_POSITIVE = "Positive";
    public final String SENTIMENT_VERY_NEGATIVE = "Very Negative";
    public final String SENTIMENT_VERY_POSITIVE = "Very Positive";

    /**
     * Conectar ao twitter
     */
    public void connect() {
        log.info(this.utils.log_info("Tentando conectar ao Twitter"));
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(this.consumerKey).setOAuthConsumerSecret(this.consumerSecrete)
                .setOAuthAccessToken(this.accessToken).setOAuthAccessTokenSecret(this.accessTokenSecret);
        TwitterFactory tf = new TwitterFactory(cb.build());
        this.twitter = tf.getInstance();
        log.info(this.utils.log_info("Conectado ao Twitter"));
    }

    /**
     * Após a conexão com o twitter, executa a busca pelos tweets de acordo com
     * o parâmetro da palavra de busca
     *
     * @param filterWord Palavra de busca
     * @param quantityTweet Quantidade de tweets como resultado
     * @return List<TweetDocument>
     * @throws TwitterException TwitterException
     */
    public List<TweetDocument> readTweet(String filterWord, int quantityTweet) throws TwitterException {
        log.info(this.utils.log_info("Tentando ler os tweets"));
        Query query = new Query(filterWord);
        query.setCount(100);
        QueryResult result = twitter.search(query);
        log.info(this.utils.log_info("Tweets lidos com sucesso"));

        List<TweetDocument> tweetList = new ArrayList<>();
        result.getTweets().forEach(t -> {
            log.info("### tweet: " + ((Status) t).getText());
            tweetList.add(
                    TweetDocument.builder()
                            .createdAt(((Status) t).getCreatedAt())
                            .idTweet(((Status) t).getId())
                            .source(((Status) t).getSource())
                            .text(((Status) t).getText())
                            .build());
        });

        return tweetList;
    }

    /**
     * Faz a análise do sentimento no texto do tweet e adiciona à propriedade
     * @param tweets List<TweetDocument>
     * @return Tweets com seu sentimento
     */
    public List<TweetDocument> analyzeSentiment(List<TweetDocument> tweets) {
        tweets.stream().forEach((TweetDocument tweet) -> {
            String sentiment = TwitterHelperImpl.this.analyzer.findSentiment(tweet.getText());
            tweet.setSentiment(sentiment);
        });
        
        return tweets;
    }

}
