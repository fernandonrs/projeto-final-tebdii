/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.mongodb.repositories;

import com.tebdii.mongodb.models.TweetDocument;
import java.io.Serializable;
import java.util.List;

/**
 * Interface para operações de CRUD com Spark
 * 
 * @author fernando
 */
public interface TweetDocumentRepository extends Serializable {
    
    public List<TweetDocument> findAll();
    public List<TweetDocument> findBy(String filter);
    public void save(List<TweetDocument> tweets);
    
}
