/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.mongodb.repositories;

import com.tebdii.mongodb.models.TweetDocument;
import com.tebdii.spark.SparkHelper;
import com.tebdii.utilities.Utilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.spark.api.java.JavaRDD;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Classe para implementação das operações de CRUD com Spark
 *
 * @author fernando
 */
@Log4j2
@Repository
public class TweetDocumentRepositoryImpl implements TweetDocumentRepository {

    @Autowired
    private SparkHelper<TweetDocument> spark;
    @Autowired
    private Utilities utils;
    private static List<Document> docs = new ArrayList<>();
      
    @Override
    public List<TweetDocument> findAll() {
        List<TweetDocument> tweets = new ArrayList<>();

        return tweets;
    }

    @Override
    public List<TweetDocument> findBy(String filter) {
        List<TweetDocument> tweets = new ArrayList<>();

        return tweets;
    }

    @Override
    public void save(List<TweetDocument> tweets) {          
        JavaRDD<TweetDocument> sparkDocument = this.spark.sparkContext().parallelize(tweets);         
        sparkDocument.foreach((TweetDocument t) -> {    
            Map<String, Object> keyValues = new HashMap<>();
            keyValues.put("topic", t.getTopic());
            keyValues.put("text", t.getText());
            keyValues.put("createdAt", t.getCreatedAt().toString());
            keyValues.put("idTweet", Long.toString(t.getIdTweet()));
            keyValues.put("source", t.getSource());
            keyValues.put("sentiment", t.getSentiment().replace(",", ""));
            Document d = new Document(keyValues);
            docs.add(d);
        }); 
        
        JavaRDD<Document> docsRDD = this.spark.sparkContext().parallelize(docs);         
        log.info(this.utils.log_info("Tentando salvar no MongoDB..."));
        this.spark.saveDocumentOnMongoDB(docsRDD);
        log.info(this.utils.log_info("Análise de sentimento salva no MongoDB..."));
    }

}
