/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.mongodb.repositories;

import com.tebdii.mongodb.models.ListingsAndReviews;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernando
 */
public interface ListingsAndReviewsRepository extends MongoRepository<ListingsAndReviews, String> { }
