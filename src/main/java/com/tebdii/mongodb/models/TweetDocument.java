/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.mongodb.models;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Entidade para as informações de um tweet
 *
 * @author fernando
 */
@Document
@Data  @NoArgsConstructor @AllArgsConstructor
@Builder
public class TweetDocument implements Serializable {

    @Id
    private long id;    
    public Date createdAt;    
    public long idTweet;    
    public String text;    
    public String source;    
    public String sentiment; 
    public String topic;
    
}
