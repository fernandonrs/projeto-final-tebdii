/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tebdii.mongodb.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author fernando
 */
@Document
@Data @NoArgsConstructor @AllArgsConstructor @Builder @EqualsAndHashCode
public class ListingsAndReviews {
    
    @Id
    public String id;
    
    @Field
    public String name;
}
