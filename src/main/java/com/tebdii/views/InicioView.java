package com.tebdii.views;

import com.tebdii.services.ListingsAndReviewsService;
import com.tebdii.spark.Teste;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouteAlias;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import twitter4j.TwitterException;

@Log4j2
@Route(value = "index", layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@PageTitle("Inicio")
public class InicioView extends HorizontalLayout {

    @Autowired private Teste testeSpark;
    @Autowired private ListingsAndReviewsService repository;
    
    private TextField name;
    private Button btnTest;
    private Button btnList;

    public InicioView() {
        addClassName("inicio-view");
        
        btnTest = new Button("Testar Twitter");
        btnList = new Button("Listar dados");
        add(btnTest, btnList);
        setVerticalComponentAlignment(Alignment.CENTER, btnTest, btnList);
                
        
    }
    
    @PostConstruct
    private void InicioViewPostContruct() {
        btnTest.addClickListener(e -> {
            try {   
                this.testeSpark.testTwitter();
                //Notification.show(String.format("Lendo Total pelo Spark : %s", this.testeSpark.getTotalFromMongoDB()));
            } catch (TwitterException ex) {
                Notification.show("Erro " + ex.getMessage());
            }            
        });
        
        btnList.addClickListener(e -> {
            try {                       
                Notification.show(String.format("Lendo Total pelo Java Spring : %s", this.repository.findAll().size()), 10000, Notification.Position.MIDDLE);
            } catch (Exception ex) {
                Notification.show("Erro " + ex.getMessage(), 100000, Notification.Position.MIDDLE);
            }            
        });
    }

}
