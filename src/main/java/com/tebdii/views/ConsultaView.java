package com.tebdii.views;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.vaadin.jchristophe.stackbar.VStackBar;

@Log4j2
@Route(value = "consulta", layout = MainView.class)
@PageTitle("Consultar")
public class ConsultaView extends HorizontalLayout {

    public ConsultaView() {
        this.creatBarGraph();
    }

    private void creatBarGraph() {
        VStackBar stackBar = new VStackBar();
        stackBar.setBars(1, 2, 3);
        stackBar.setDescriptions("Negativa", "Neutra", "Positiva");
        this.add(stackBar);
    }

    private void createDataTable() {

    }

    @PostConstruct
    private void InicioViewPostContruct() {
    }

}
