package com.tebdii.views;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.tebdii.views.MainView;
import com.vaadin.flow.component.charts.model.HorizontalAlign;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

@Route(value = "sobre", layout = MainView.class)
@PageTitle("Sobre")
public class SobreView extends Div {

    private HorizontalLayout createCenterBox() {
        HorizontalLayout hz = new HorizontalLayout();
        hz.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        hz.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        hz.setMargin(true);
        hz.setPadding(true);
        hz.setWidth("80%");
        hz.getStyle().set("border", "1px solid white");
        
        return hz;
    }
    
    public SobreView() {
        addClassName("sobre-view");
        add(this.createCenterBox());
    }

}
